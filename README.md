# Iban manager

## Run the container:

1. Install [docker-compose](https://docs.docker.com/compose/install/)
2. docker-compose up

## Run tests:

docker-compose -f docker-compose-test.yml up --force-recreate

After the test have finished you can check the coverage in htmlcov/index.html

## Google oauth2 configuration

Go to [Google API console](https://console.cloud.google.com/apis)

1. Select or create a project to activate oauth.
2. Go to Credentials.
3. Create oauth credentials.
4. Select "Client OAuth ID" and App Type -> Other
6. Copy id and secret, paste these values in SOCIAL_AUTH_GOOGLE_OAUTH2_KEY and SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET in Django settings.
7. Include in Django settings in LOGIN_REDIRECT_URL a valid url for the django app (that will be the url opened after the login process).
8. Go to Control Panel and make sure that Google+ API is enabled.
9. In the project directory run `docker-compose up`
10. Check the login process using the url http://localhost:8000/auth/login/google-oauth2/


## Get valid Access Token for the application

This is valid only as testing method.

1. Go to /admin console in the app
2. In DJANGO OAUTH TOOLKIT section add a new value in Applications
3. In User select a superuser for the application (if there is no superuser, [create one](https://docs.djangoproject.com/en/2.1/ref/django-admin/)
4. In Authorization grant type select "Client credentials"
5. Include a name and copy the client id value -> CLIENT_OAUTH_ID.
6. Retrieve the value of SOCIAL_AUTH_GOOGLE_OAUTH2_KEY from the settings file.
7. In a browser launch https://accounts.google.com/o/oauth2/auth?client_id=<google_oauth2_key>&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=openid+email+profile+https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/plus.me+https://www.googleapis.com/auth/userinfo.email&response_type=code substituting the value of <google_oauth_key> with SOCIAL_AUTH_GOOGLE_OAUTH2_KEY.
8. Copy the ACCESS_CODE.
9. Using SOCIAL_AUTH_GOOGLE_OAUTH2_KEY and SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET launch in a terminal: curl --request POST --data "code=$code&client_id=<SOCIAL_AUTH_GOOGLE_OAUTH_KEY>&client_secret=SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET>&redirect_uri=urn:ietf:wg:oauth:2.0:oob&grant_type=authorization_code" https://www.googleapis.com/oauth2/v3/token
8. The response will contain a token in json format, we need the access_token field -> GOOGLE_TOKEN.
9. With the value of the access token field, we convert the google token in a valid app token: curl --request POST --data "grant_type=convert_token&client_id=CLIENT_OAUTH_ID&backend=google-oauth2&token=GOOGLE_TOKEN" http://localhost:8000/auth/convert-token
10. We will have a valid token to use in our app: curl -H "Authorization: Bearer 9oEIytPKsENOThFaMJNzojHxZSst4Z" -X GET http://localhost:8000/clients_data/


