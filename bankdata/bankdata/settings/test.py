import os

from .settings import *

db_url = os.environ['DATABASE_URL']
DATABASES = {
    'default': {
        'ENGINE': 'django_prometheus.db.backends.sqlite3',
        'NAME': 'admin',
        'USER': 'admin',
        'HOST': db_url,
        'PORT': 5432,
    }
}
