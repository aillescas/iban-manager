from rest_framework import serializers
from schwifty import IBAN

from client_management.models import BankData


class BankDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankData
        fields = ('__all__')
        read_only_fields = ('id', 'owner')

    def validate_iban(self, value):
        try:
            iban = IBAN(value)
        except ValueError as value_error:
            raise serializers.ValidationError(str(value_error))
        return iban.compact
