from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated

from client_management.models import Client
from client_management.serializers.clients import ClientSerializer


class ClientCreation(CreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permissions_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
