from rest_framework import permissions
from rest_framework.generics import CreateAPIView, DestroyAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from client_management.models import BankData
from client_management.serializers.bankdata import BankDataSerializer


class IsUserOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        print("Usuarios -> {} {}".format(obj.owner, request.user))
        return (obj.owner is None or obj.owner == request.user) and request.user


class BankDataList(APIView):
    """
    List all BankData for the user
    """
    def get(self, request, format='json'):
        bank_data_list = BankData.objects.filter(owner=request.user)
        serializer = BankDataSerializer(bank_data_list, many=True)
        return Response(serializer.data)


class BaseManageView(APIView):
    """
    The base class for ManageViews
        A ManageView is a view which is used to dispatch the requests to the appropriate views
        This is done so that we can use one URL with different methods (GET, PUT, etc)
    """
    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self, 'VIEWS_BY_METHOD'):
            msg = 'VIEWS_BY_METHOD static dictionary variable must be defined on a ManageView class!'
            raise NotImplementedError(msg)
        if request.method in self.VIEWS_BY_METHOD:
            return self.VIEWS_BY_METHOD[request.method]()(request, *args, **kwargs)

        return Response(status=405)


class BankDataDestroyView(DestroyAPIView):
    queryset = BankData.objects.all()
    serializer_class = BankDataSerializer
    permissions_classes = (IsUserOwner,)


class BankDataUpdateView(UpdateAPIView):
    queryset = BankData.objects.all()
    serializer_class = BankDataSerializer
    permissions_classes = (IsUserOwner,)


class BankDataDetailsView(RetrieveAPIView):
    queryset = BankData.objects.all()
    serializer_class = BankDataSerializer
    permissions_classes = (IsUserOwner,)


class BankDataManageView(BaseManageView):

    VIEWS_BY_METHOD = {
        'DELETE': BankDataDestroyView.as_view,
        'GET': BankDataDetailsView.as_view,
        'PUT': BankDataUpdateView.as_view,
        'PATCH': BankDataUpdateView.as_view
    }


class BankDataCreation(CreateAPIView):
    queryset = BankData.objects.all()
    serializer_class = BankDataSerializer
    permissions_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
