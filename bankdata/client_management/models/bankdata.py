from django.db import models


class BankData(models.Model):
    client = models.ForeignKey('client_management.client', on_delete=models.PROTECT)
    iban = models.CharField(max_length=30)
    swift = models.CharField(max_length=11)
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey('auth.User', on_delete=models.PROTECT)

    def __repr__(self):
        return "{} - {}".format(self.client.client_name, self.iban)

    def __str__(self):
        return self.__repr__()

    class Meta:
        verbose_name_plural = "Bank Data"
        ordering = ('created',)
