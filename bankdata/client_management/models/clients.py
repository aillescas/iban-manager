from django.db import models


class Client(models.Model):

    client_name = models.CharField(max_length=100)
    passport_number = models.CharField(max_length=20)

    class Meta:
        ordering = ('client_name',)
