from django.contrib import admin

from .models import Client, BankData

admin.site.register(Client)
admin.site.register(BankData)
