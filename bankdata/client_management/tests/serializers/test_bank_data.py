from django.contrib.auth.models import User
from rest_framework.test import APITestCase

from client_management.models.bankdata import BankData
from client_management.models.clients import Client
from client_management.serializers.bankdata import BankDataSerializer


class BankDataSerializerTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(username='admin', email='admin@admin.es')
        self.bank_client = Client.objects.create(client_name='test_name', passport_number='test_passport')

    def test_bankdata_serializer_create(self):
        new_data = {
            'client': self.bank_client.pk,
            'iban': 'ES9121000418450200051332',
            'swift': 'new_swift',
        }

        bank_data_serializer = BankDataSerializer(data=new_data)
        self.assertTrue(bank_data_serializer.is_valid())
        bank_data_serializer.save(owner=self.user)
        self.assertTrue(BankData.objects.filter(client=self.bank_client).count() == 1)

    def test_bankdata_serializer_invalid_iban(self):
        new_data = {
            'client': self.bank_client.pk,
            'iban': 'Invalid',
            'swift': 'new_swift',
        }

        bank_data_serializer = BankDataSerializer(data=new_data)
        self.assertFalse(bank_data_serializer.is_valid())
