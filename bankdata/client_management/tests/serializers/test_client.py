from rest_framework.test import APITestCase

from client_management.models.clients import Client
from client_management.serializers.clients import ClientSerializer


class ClientSerializerTests(APITestCase):
    def test_client_serializer_create(self):
        new_data = {
            'client_name': 'client',
            'passport_number': 'number',
        }

        self.assertFalse(Client.objects.filter(client_name='client', passport_number='number').exists())

        client_serializer = ClientSerializer(data=new_data)
        self.assertTrue(client_serializer.is_valid())
        client_serializer.save()

        self.assertTrue(Client.objects.filter(client_name='client', passport_number='number').exists())
