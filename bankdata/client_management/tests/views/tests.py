from unittest.mock import patch

from django.contrib.auth.models import User
from django.utils import timezone
from oauth2_provider.models import get_access_token_model, get_application_model
from rest_framework.test import APITestCase

from client_management.models.bankdata import BankData
from client_management.models.clients import Client
from client_management.serializers.bankdata import BankDataSerializer


Application = get_application_model()
AccessToken = get_access_token_model()


@patch('client_management.views.bankdata.IsUserOwner')
class BankDataTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(username='admin', email='admin@admin.es')
        self.client_bank = Client.objects.create(client_name='test_name', passport_number='test_passport')

        self.application = Application.objects.create(
            name="Test Application",
            redirect_uris="http://localhost http://example.com http://example.org",
            user=self.user,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
        )

        self.access_token = AccessToken.objects.create(
            user=self.user,
            scope="read write",
            expires=timezone.now() + timezone.timedelta(seconds=300),
            token="secret-access-token-key",
            application=self.application
        )
        # read or write as per your choice
        self.access_token.scope = "read write"
        self.access_token.save()

        # correct token and correct scope
        self.auth = "Bearer {0}".format(self.access_token.token)

    def test_can_get_bankdata_details(self, _):
        bank_data = BankData.objects.create(iban='ES7921000813610123456789',
                                            swift='test_swift', client=self.client_bank,
                                            owner=self.user)
        response = self.client.get(f'/client_data/{bank_data.id}/', HTTP_AUTHORIZATION=self.auth)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, BankDataSerializer(instance=bank_data).data)

    def test_can_delete_bankdata(self, _):
        bank_data = BankData.objects.create(iban='ES7921000813610123456789',
                                            swift='test_swift', client=self.client_bank,
                                            owner=self.user)
        response = self.client.delete(f'/client_data/{bank_data.id}/', HTTP_AUTHORIZATION=self.auth)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(BankData.objects.count(), 0)

    def test_can_update_bankdata(self, _):
        bank_data = BankData.objects.create(iban='ES7921000813610123456789',
                                            swift='test_swift', client=self.client_bank,
                                            owner=self.user)
        update_data = {
            'iban': 'ES9121000418450200051332',
            'swift': 'new_swift',
        }
        response = self.client.patch(f'/client_data/{bank_data.id}/',
                                     data=update_data, HTTP_AUTHORIZATION=self.auth)
        bank_data.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(bank_data.iban, update_data.get('iban'))
        self.assertEqual(bank_data.swift, update_data.get('swift'))
